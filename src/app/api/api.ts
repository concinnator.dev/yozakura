export interface Post {
  id: number;
  title: string;
  content: string;
  createdAt: string;
}

export interface CreatePostDto {
  title: string;
  content: string;
}

export interface UpdatePostDto {
  title?: string;
  content?: string;
}

export interface RegisterDto {
  email: string;
  password: string;
  rememberMe: boolean;
}

export interface LoginDto {
  email: string;
  password: string;
  rememberMe: boolean;
}

const API_BASE_URL = 'http://localhost:8080/api/posts'; 

export const fetchPosts = async (): Promise<Post[]> => {
  const response = await fetch(API_BASE_URL);
  if (!response.ok) {
      throw new Error('Erro ao buscar posts');
  }
  const data: Post[] = await response.json();
  return data;
};

export const fetchPostById = async (postId: number): Promise<Post | null> => {
  const response = await fetch(`${API_BASE_URL}/${postId}`);
  if (!response.ok) {
      return null;
  }
  const data: Post = await response.json();
  return data;
};

export const createPost = async (postData: CreatePostDto): Promise<void> => {
  const response = await fetch(API_BASE_URL, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(postData),
  });

  if (!response.ok) {
      throw new Error('Erro ao criar o post');
  }
};

export const updatePost = async (postId: number, postData: UpdatePostDto): Promise<void> => {
  const response = await fetch(`${API_BASE_URL}/${postId}`, {
      method: 'PUT',
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(postData),
  });

  if (!response.ok) {
      throw new Error('Erro ao atualizar o post');
  }
};

export const deletePost = async (postId: number): Promise<void> => {
  const response = await fetch(`${API_BASE_URL}/${postId}`, {
      method: 'DELETE',
  });

  if (!response.ok) {
      throw new Error('Erro ao excluir o post');
  }
};

const API_BASE_URL1 = 'http://localhost:8080/auth/Account';

export const registerUser = async (registerData: RegisterDto): Promise<void> => {
  const response = await fetch(`${API_BASE_URL1}/register`, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(registerData),
  });

  if (!response.ok) {
      throw new Error('Erro ao registrar usuário');
  }
};


export const loginUser = async (loginData: LoginDto): Promise<{ token: string }> => {
  const response = await fetch(`${API_BASE_URL1}/login`, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(loginData),
  });

  if (!response.ok) {
      throw new Error('Erro ao realizar login');
  }

  const responseData = await response.json();
  const { token } = responseData; 

  return { token };
};
