export default function Sobre() {
    return (
        <div className="flex flex-col min-h-screen">
            <main className="flex flex-grow">
                <article className="flex-grow p-4">
                    <h2 className="text-2xl font-bold mb-4">Sobre</h2>
                    <p>Bem-vindo! Somos uma equipe apaixonada por compartilhar conhecimento e inspirar outras pessoas. Neste blog, você encontrará uma variedade de artigos sobre diversos temas, desde tecnologia até dicas de estilo de vida. Nosso objetivo é fornecer conteúdo de qualidade que informe, eduque e entretenha nossos leitores.</p>
                    <p>Nossa equipe está empenhada em manter este blog atualizado com novos artigos regularmente. Esperamos que você desfrute da sua visita e encontre informações úteis aqui.</p>
                </article>
            </main>
        </div>
    );
}

