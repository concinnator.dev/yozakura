import React from 'react';
import Posts from '../components/Posts'; 
import Footer from '../components/footer'; 
import './../src/app/globals.css';
import Navbar from '../components/navbar';
const HomePage = () => {
  const isLoggedIn = false;
  return (
    <div>

<Navbar isLoggedIn={isLoggedIn} />
    <div className="container mx-auto p-4">
      <Posts/> 
      <Footer/>
    </div>  
    </div>
  );
};
export default HomePage;
