import Navbar from '../../components/navbar';
import Footer from '../../components/footer';
import { useAuth } from '../../src/app/context/AuthContext'; 
import { useRouter } from 'next/router';


const ClientePage = () => {
  const { token, logout } = useAuth(); 
  const router = useRouter();

  const handleLogout = () => {
    logout(); 
    router.push('/'); 
  };

  return (
    <div>
      <Navbar isLoggedIn={true} />
      <div className="container mx-auto p-4">
        <h2 className="text-2xl font-bold mb-4">Área do Cliente</h2>
        <p>Bem-vindo à sua área do cliente!</p>
        <button
          onClick={handleLogout}
          className="mt-4 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
        >
          Logout
        </button>
      </div>
      <Footer />
    </div>
  );
};

export default ClientePage;
