import React from 'react';
 export default function Footer () {
    return (
        <footer className="bg-gray-800 text-white text-center py-5 w-full bottom-0"> 
            <p>&copy; 2024 Blog Yozakura. Todos os direitos reservados.</p> 
        </footer> 
    );
}


