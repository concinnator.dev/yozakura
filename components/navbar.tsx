import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import './../src/app/globals.css';
import { IoMdLogOut } from "react-icons/io";

import { IoLogInOutline } from "react-icons/io5";
interface NavbarProps {
    isLoggedIn: boolean;
}

const Navbar: React.FC<NavbarProps> = ({ isLoggedIn }) => {
    const router = useRouter();

    const handleLogout = () => {
        router.push('/');
    };

    return (
        <nav className="bg-gray-800 text-white py-4">
            <div className="container mx-auto flex justify-between items-center">
                <div className="flex items-center">
                    <Link href="/">
                        Blog Yozakura
                    </Link>
                </div>
                <div className="flex items-center">
                    {isLoggedIn ? (
                        <>
                            <Link href="/pages/perfil.tsx">
                                Bem-Vindo
                            </Link>
                            <button onClick={handleLogout}><IoMdLogOut /></button>
                        </>
                    ) : (
                        <>
                            <Link href="/LoginPage">
                                Login
                            </Link>
                            <Link href="/RegisterPage">
                                Register
                            </Link>
                        </>
                    )}
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
