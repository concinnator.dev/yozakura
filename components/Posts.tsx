import React, { useEffect, useState } from 'react';
import { fetchPosts } from '../src/app/api/api'; 
import './../src/app/globals.css';

interface Post {
  id: number;
  title: string;
  content: string;
  createdAt: string;
}

const Posts = () => {
  const [posts, setPosts] = useState<Post[]>([]);

  useEffect(() => {
    const fetchAllPosts = async () => {
      try {
        const fetchedPosts = await fetchPosts();
        setPosts(fetchedPosts);
      } catch (error) {
        console.error('Erro ao buscar posts:', error);
      }
    };
    fetchAllPosts();
  }, []);

  return (
    <div>
      <h2 className="text-2xl font-bold mb-4">Últimos Posts</h2>
      <ul>
        {posts.map(post => (
          <li key={post.id}>
            <h3>{post.title}</h3>
            <p>{post.content}</p>
            <p>Publicado em: {new Date(post.createdAt).toLocaleString()}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Posts;
